import React from 'react';
import { Button, Typography, Stack, Grid, Box } from '@mui/material';
import { LocalStorageKeys } from '../../utils';
import { useNavigate } from 'react-router-dom';
import { AppRoutes } from '../../router/routes';
import { LoginSuccess } from "../../router/access";
import { useStyles } from './style';
import { TextBox, Password } from '../../components';

export const Login = ({
    title = "Sign in",
    login = "Log In"
}) => {

    const navigate = useNavigate();
    const classes = useStyles();

    const onLogin = () => {
        localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
        navigate(AppRoutes.dashboardCard);
    }

    React.useEffect(() => {
        if (localStorage.getItem(LocalStorageKeys.authToken)) {
            navigate(LoginSuccess())
        }
        console.log("here")
    })

    return <div className={classes.root}>

        <Grid container>
            {/* login card */}
            <Grid item xs={12} sm={12} md={12} lg={12} >
                <Stack className={classes.backgroundImg}>
                    <Box className={classes.card}>
                        {/* title */}
                        <Typography className={classes.signIn}>
                            {title}
                        </Typography>
                        {/* title */}
                        {/* Inputbox */}
                        <Stack className={classes.inputBox} >
                            <TextBox label="Mobile Number / Email ID" placeholder="Balaganesh@gmail.com" />
                        </Stack>
                        {/* Inputbox */}

                        {/* password */}
                        <Stack className={classes.passWord} >
                            <Password />
                        </Stack>
                        {/* password */}

                        {/* forget password */}
                        <Typography className={classes.forget}>
                            Did you forget your password? <span className={classes.clickHere}>Click Here</span>
                        </Typography>
                        {/* forget password */}

                        {/* powered by */}
                        <Stack direction="row" spacing={1} alignItems="center" justifyContent="center" className={classes.sponsors}>
                            <Typography className={classes.powerBy}>
                                Powered by
                            </Typography>
                            <img src="images/logo@2x.png" alt="logo" className={classes.squareLogo} />
                            <Typography className={classes.propAuto}>
                                Property Automate
                            </Typography>
                        </Stack>
                        {/* powered by */}

                        {/* login button */}
                        <Button variant="contained" fullWidth className={classes.login} onClick={onLogin} type="submit" >
                            {login}
                        </Button>
                        {/* login button */}
                    </Box>
                </Stack>
            </Grid>
            {/* login card */}
        </Grid>
    </div>
}
