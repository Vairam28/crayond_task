import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    card: {
        backgroundColor: "#FFFFFF",
        boxShadow: "0px 8px 24px #0000001F",
        borderRadius: "16px",
        opacity: 1,
        padding: "30px",
        marginRight: "105px",
        width: "416px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "0px",
            width: "auto",
        }
    },
    signIn: {
        fontSize: "24px",
        textAlign: "left",
        marginBottom: "32px",
        fontWeight: "bold"
    },
    inputBox: {
        marginBottom: "30px"
    },
    passWord: {
        marginBottom: "8px"
    },
    forget: {
        fontSize: "12px",
        textAlign: "right",
        marginBottom: "50px",
        marginTop: "8px"
    },
    clickHere: {
        color: "#5078E1",
        cursor: "pointer"
    },
    powerBy: {
        color: "#98A0AC",
        fontSize: "10px"
    },
    propAuto: {
        color: "#4E5A6B",
        fontSize: "12px"
    },
    sponsors:{
        marginBottom:"24px"
    },
    login:{
        borderRadius:"12px",
        backgroundColor:"#5078E1",
        textTransform: "initial"
    },
    squareLogo:{
        width: "24px",
        height: "17px",
    },
    backgroundImg:{
        alignItems:"end",
        backgroundImage:`url("images/mainbackgroundimage.png")`,
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        [theme.breakpoints.down("sm")]: {
            alignItems:"center",
        }
    }

}))