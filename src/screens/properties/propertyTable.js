import React from 'react';
import { Typography, Box, Stack, Grid } from '@mui/material';
import { useStyles } from "./style";
import { LocalStorageKeys } from '../../utils';
import { useNavigate } from 'react-router-dom';
import { AppRoutes } from '../../router/routes';
import { CustomizeTable, Search, FilterIcon } from '../../components';


export const TableProperty = props => {

    const navigate = useNavigate();
    const classes = useStyles();


    const onLogin = () => {
        localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
        navigate(AppRoutes.home);
    }



    return <Box className={classes.root}>
        <Stack direction="row" spacing={1} alignItems="center" className={classes.headerCreate}>
            <Typography className={classes.headerText}>
                Properties
            </Typography>
        </Stack>
        {/* property header */}
        {/* requests/maintaince and property table */}
        <Box className={classes.createGraphCard}>
            <Grid container spacing={2} >
                {/* property table */}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Box className={classes.propertyDetails}>
                        <Stack>
                            <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pb={2}>
                                <Search />
                                <FilterIcon />
                            </Stack>
                            <Stack className={classes.tableWithAddIcon}>
                                <CustomizeTable />
                            </Stack>
                        </Stack>
                    </Box>
                </Grid>
                {/* property table */}
            </Grid >
        </Box >
        {/* requests/maintaince and property table */}
    </Box >
}