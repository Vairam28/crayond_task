import React from "react";
import { TableProperty } from './propertyTable';
import { withNavBars } from "./../../HOCs";

class PropTable extends React.Component {
  render() {
    return <TableProperty />;
  }
}

export default withNavBars(PropTable);