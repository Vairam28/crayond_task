import React from "react";
import { UpdatedDetails } from './updatedDetails';
import { withNavBars } from "./../../HOCs";

class UpdatedScreen extends React.Component {
  render() {
    return <UpdatedDetails />;
  }
}

export default withNavBars(UpdatedScreen);