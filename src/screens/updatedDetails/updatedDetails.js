import React from 'react';
import { Typography, Box, Stack, Grid } from '@mui/material';
import { useStyles } from "./style";
import { LocalStorageKeys } from '../../utils';
import { useNavigate } from 'react-router-dom';
import { AppRoutes } from '../../router/routes';
import { DownArrow } from '../../assets/downArrow';
import { AddressIcon, ContactUs } from '../../components';

export const UpdatedDetails = props => {

    const navigate = useNavigate();
    const classes = useStyles();

    var textMapping = [
        {
            label: "Company Name",
            field: "Company Name",
            size: {
                lg: 2,
                md: 2,
                sm: 4,
                xs: 6
            }
        },
        {
            label: "Property Name",
            field: "Rubix Appartment",
            size: {
                lg: 2,
                md: 2,
                sm: 4,
                xs: 6
            }
        },
        {
            label: "Property Type",
            field: "Property Type",
            size: {
                lg: 2,
                md: 2,
                sm: 4,
                xs: 6
            }
        },
        {
            label: "Property Purpose",
            field: "Residential",
            size: {
                lg: 2,
                md: 2,
                sm: 4,
                xs: 6
            }
        },
        {
            label: "Payment Period",
            field: "Monthly",
            size: {
                lg: 2,
                md: 2,
                sm: 4,
                xs: 6
            }
        },
        {
            label: "Status",
            field: "Active",
            size: {
                lg: 2,
                md: 2,
                sm: 4,
                xs: 6
            }
        },
        {
            label: "Property Description",
            field: "A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, a street bike available at a starting price of Rs. 1,62,916 in India. It is available in 3 variants and 8 colours with top variant price starting from The Yamaha",
            size: {
                lg: 12,
                md: 12,
                sm: 12,
                xs: 12
            }
        },
    ]

    var areaMapping = [
        {
            label: "Revenue Type",
            field: "Lease",
            sqFeet: "",
            size: {
                lg: 1.7,
                md: 1.7,
                sm: 3,
                xs: 6
            }
        },
        {
            label: "Carpet Area",
            field: "10000",
            sqFeet: "Sq. Ft",
            size: {
                lg: 1.7,
                md: 1.7,
                sm: 3,
                xs: 6
            }
        },
        {
            label: "Total Area",
            field: "165480",
            sqFeet: "Sq. Ft",
            size: {
                lg: 1.7,
                md: 1.7,
                sm: 3,
                xs: 6
            }
        },
        {
            label: "Year Built",
            field: "22-02-2020",
            sqFeet: "",
            size: {
                lg: 1.7,
                md: 1.7,
                sm: 3,
                xs: 6
            }
        },
        {
            label: "Handover Date",
            field: "22-02-2022",
            sqFeet: "",
            size: {
                lg: 1.7,
                md: 1.7,
                sm: 3,
                xs: 6
            }
        },
        {
            label: "Public Listing",
            field: "Public",
            sqFeet: "",
            size: {
                lg: 1.7,
                md: 1.7,
                sm: 3,
                xs: 6
            }
        },
        {
            label: "Pets Allowed",
            field: "Yes",
            sqFeet: "",
            size: {
                lg: 1.7,
                md: 1.7,
                sm: 3,
                xs: 6
            }
        },
    ]

    var contactUs = [
        {
            label: "Business Phone :",
            field: "044 23224944",
            size: {
                lg: 6,
                md: 6,
                sm: 6,
                xs: 12
            }
        },
        {
            label: "Mobile Phone :",
            field: "044 23224944",
            size: {
                lg: 6,
                md: 6,
                sm: 6,
                xs: 12
            }
        },
        {
            label: "Website :",
            field: "propertyautomate.com",
            size: {
                lg: 6,
                md: 6,
                sm: 6,
                xs: 12
            }
        },
        {
            label: "Email Address :",
            field: "mail@propertyautomate.com",
            size: {
                lg: 6,
                md: 6,
                sm: 6,
                xs: 12
            }
        }
    ]

    const onBackToCreate = () => {
        localStorage.removeItem(LocalStorageKeys.authToken);
        navigate(AppRoutes.home);
    }


    return <Box className={classes.root}>
        {/* property header */}
        <Stack direction="row" spacing={1} alignItems="center" className={classes.headerCreate}>
            <Box className={classes.arrowColor}>
                <DownArrow onClick={onBackToCreate} />
            </Box>
            <Typography className={classes.headerText}>
                Rubix Appartment
            </Typography>
        </Stack>
        {/* property header */}
        {/* property details */}
        <Box className={classes.createPortion}>
            <Grid container className={classes.propertyDetails} direction="row" justifyContent="center" alignItems="center">
                <Grid xs={12} sm={3} md={2} p={2} pr={0}>
                    <Stack className={classes.parentViwedImg}>
                        <img src="images/viewed image.png" alt="view" className={classes.viewedImg} />
                    </Stack>
                </Grid>
                <Grid xs={12} sm={9} md={10} p={2}>
                    <Typography className={classes.propertyHeader} mb={2}>
                        PROPERTY DETAILS
                    </Typography>
                    <Grid container spacing={2} mb={1}>
                        {textMapping?.map((val) => {
                            return (
                                <Grid xs={val?.size?.xs} sm={val?.size?.sm} md={val?.size?.md} lg={val?.size?.lg} p={2} pb={0}>
                                    <Stack>
                                        <Typography className={classes.label} > {val.label} </Typography>
                                        <Typography className={classes.savedData} > {val.field} </Typography>
                                    </Stack>
                                </Grid>
                            )
                        })
                        }
                    </Grid>
                </Grid>
            </Grid>
        </Box>
        {/* property details */}
        {/* property date and types */}
        <Box className={classes.addressType}>
            <Grid container className={classes.propertyImg} >
                {areaMapping?.map((val) => {
                    return (
                        <Grid xs={val?.size?.xs} sm={val?.size?.sm} md={val?.size?.md} lg={val?.size?.lg} p={2}>
                            <Stack>
                                <Typography className={classes.label} > {val.label} </Typography>
                                <Typography className={classes.savedData} > {val.field} <span className={classes.squareFeet}>{val.sqFeet}</span> </Typography>
                            </Stack>
                        </Grid>
                    )
                })
                }
            </Grid>
        </Box>
        {/* property date and types */}
        {/* address */}
        <Box className={classes.addressType}>
            <Grid container>
                <Grid item xs={12} className={classes.propertyImg} >
                    <Stack p={2}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6} md={4}>
                                <Stack>
                                    <img src="/images/mapDemo.png" alt="map" className={classes.map} />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={6} md={3}>
                                <Stack className={classes.addressParent}>
                                    <Stack direction="row" alignItems={"center"} mb={1.5} >
                                        <AddressIcon />
                                        <Typography ml={1} className={classes.addressHeader} >ADDRESS</Typography>
                                    </Stack>
                                    <Typography className={classes.addressSubHeader} mb={2.5}>
                                        23 Main Street, 3rd Cross street, 3rd Sector, Chennai, Tamilnadu, India -60001
                                    </Typography>
                                    <Typography className={classes.addressLongitude} mb={1.5}>
                                        Latitude : <span className={classes.addressDegree}> 27.2046° N</span>
                                    </Typography>
                                    <Typography className={classes.addressLongitude}>
                                        Longitude :  <span className={classes.addressDegree}> 77.4977° E</span>
                                    </Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={12} md={5}>
                                <Stack className={classes.contactParent}>
                                    <Stack direction="row" alignItems={"center"} p={1.5} pb={1.5}>
                                        <ContactUs />
                                        <Typography ml={1} className={classes.addressHeader} >CONTACT & OTHER INFORMATION</Typography>
                                    </Stack>
                                    <Grid container >
                                            {contactUs?.map((val) => {
                                                return (
                                                    <Grid xs={val?.size?.xs} sm={val?.size?.sm} md={val?.size?.md} lg={val?.size?.lg} p={1.5} pt={0} pb={3}>
                                                        <Stack>
                                                            <Typography className={classes.addressLongitude} > {val.label} </Typography>
                                                            <Typography className={classes.addressSubHeader} > {val.field} </Typography>
                                                        </Stack>
                                                    </Grid>
                                                )
                                            })
                                            }
                                        </Grid>
                                </Stack>
                            </Grid>
                        </Grid>
                    </Stack>
                </Grid>
            </Grid>
        </Box>
        {/* address */}

    </Box>
}