import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    card: {
        background: "#FFFFFF 0% 0 % no - repeat padding- box",
        boxShadow: "0px 8px 24px #0000001F",
        borderRadius: "16px",
        opacity: 1,
        padding: "36px",
        marginRight: "50px",
        marginTop: "40px",
        width: "416px",
        marginBottom: "20px",
        height: "478px"
    },
    signIn: {
        fontSize: "24px",
        textAlign: "left",
        marginBottom: "32px",
        fontWeight: "bold"
    },
    inputBox: {
        marginBottom: "30px"
    },
    passWord: {
        marginBottom: "8px"
    },
    forget: {
        fontSize: "12px",
        textAlign: "right",
        marginBottom: "50px",
        marginTop: "8px"
    },
    clickHere: {
        color: "#5078E1",
        cursor: "pointer"
    },
    powerBy: {
        color: "#98A0AC",
        fontSize: "10px"
    },
    propAuto: {
        color: "#4E5A6B",
        fontSize: "12px"
    },
    sponsors: {
        marginBottom: "24px"
    },
    login: {
        borderRadius: "12px",
        backgroundColor: "#5078E1",
        textTransform: "initial"
    },
    squareLogo: {
        width: "24px",
        height: "17px",
    },
    headerCreate: {
        boxShadow: "0px 10px 25px #0000000A",
        background: "#FFFFFF 0% 0% no-repeat padding-box",
        padding: "8px 22px",
        marginTop: "62px"
    },
    footerButton: {
        boxShadow: "0px 10px 25px #0000000A",
        background: "#FFFFFF 0% 0% no-repeat padding-box",
        padding: "11px 22px",
        justifyContent: "end",
        position: "fixed",
        bottom: "0",
        width: "95%",
        [theme.breakpoints.down("sm")]: {
            justifyContent: "center",
            width: "100%",
        }
    },
    arrowColor: {
        color: "#091B29",
        backgroundColor: "#E4E8EE",
        width: "24px",
        height: "24px",
        borderRadius: "50%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    label: {
        color: "#98A0AC",
        fontSize: "12px",
        marginBottom: "5px"
    },
    headerText: {
        color: "#091B29",
        fontSize: "16px",
        fontWeight: "bold",
    },
    createPortion: {
        backgroundColor: "#F2F4F7",
        padding: "20px 20px 20px 20px"
    },
    propertyImg: {
        boxShadow: "0px 10px 25px #0000000A",
        borderRadius: "8px",
        backgroundColor: "white",
    },
    propertyDetails: {
        boxShadow: "0px 10px 25px #0000000A",
        borderRadius: "8px",
        backgroundColor: "white",
        margin: "0 0 0 auto",
    },
    addressType: {
        backgroundColor: "#F2F4F7",
        padding: "0px 20px 20px 20px"
    },
    mapType: {
        backgroundColor: "#F2F4F7",
        padding: "0px 20px 20px 20px",
        marginBottom: "60px"
    },
    uploadingImage: {
        color: "#4E5A6B",
        fontSize: "12px",
        textAlign: "center"
    },
    propertyHeader: {
        color: "#4E5A6B",
        fontSize: "12px",
        fontWeight:"bold"
    },
    iconBuilding: {
        backgroundColor: "#F2F4F7",
        borderRadius: "50%",
        width: "111px",
        height: "111px",
        display: "flex",

    },
    Quill: {
        "& .ql-toolbar.ql-snow": {
            borderRadius: "10px 10px 0px 0px",
        },
        "& .ql-container.ql-snow": {
            borderRadius: "0px 0px 10px 10px",
        }

    },
    containedButton: {
        border: `1px solid ${theme.palette.secondary.main}`,
        backgroundColor: `${theme.palette.secondary.main}`,
        color: "white",
        textTransform: "capitalize",
        borderRadius: "10px",
        boxShadow: "none"
    },
    outlinedButton: {
        border: "1px solid #E4E8EE",
        color: "black",
        textTransform: "capitalize",
        borderRadius: "10px",
    },
    checkboxStyle:{
        "& .css-qwakhl-MuiButtonBase-root-MuiCheckbox-root.Mui-checked, .css-qwakhl-MuiButtonBase-root-MuiCheckbox-root.MuiCheckbox-indeterminate":{
            color:"black"
        }
    },
    viewedImg:{
      width:"141px"  
    },
    parentViwedImg:{
        width: "92%",
        borderRight:"1px solid #98A0AC",
        [theme.breakpoints.down("sm")]: {
            borderRight: "none",
            alignItems: "center",
        }
    },
    savedData:{
        color:"#091B29",
        fontSize:"14px",
        fontWeight: "bold",
    },
    squareFeet:{
        color:"#98A0AC",
        fontSize:"12px",
        fontWeight: "100",
        marginLeft:"4px"
    },
    addressParent:{
        border:"1px solid #E4E8EE",
        borderRadius:"8px",
        padding:"16px",
        height: "193px"
    },
    addressHeader:{
        fontSize:"12px",
        color:"#091B29",
        fontWeight: "bold",
    },
    addressSubHeader:{
        fontSize:"14px",
        color:"#091B29",
        fontWeight: "bold",
    },
    addressLongitude:{
        fontSize:"12px",
        color:"#98A0AC",
        fontWeight: "bold",
    },
    addressDegree:{
        fontSize:"12px",
        color:"#091B29",
        fontWeight: "bold",
    },
    map:{
        borderRadius:"12px",
        height: "193px"
    },
    contactParent:{
        border:"1px solid #E4E8EE",
        borderRadius:"8px",
        height: "193px",
        [theme.breakpoints.down("sm")]: {
            height: "100%",
        }
    },
}))