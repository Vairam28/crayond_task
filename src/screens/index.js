export { default as NotFound } from "./notFound";
export { default as Login } from "./login";
export { default as Home } from "./home";
export { default as UpdatedScreen } from "./updatedDetails";
export { default as DashboardCard } from "./dashboard";
export { default as PropTable } from "./properties"