import React from "react";
import { Dashboard } from './dashboard';
import { withNavBars } from "./../../HOCs";

class DashboardCard extends React.Component {
  render() {
    return <Dashboard />;
  }
}

export default withNavBars(DashboardCard);