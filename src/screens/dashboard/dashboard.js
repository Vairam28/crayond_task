import React from 'react';
import { Typography, Box, Stack, Grid, Divider } from '@mui/material';
import { useStyles } from "./style";
import { LocalStorageKeys } from '../../utils';
import { useNavigate } from 'react-router-dom';
import { AppRoutes } from '../../router/routes';
import { GraphCard, Search, Outline, ShowingIcon, CustomizeTable, AddIconImg } from '../../components';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import PropTypes from 'prop-types';


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export const Dashboard = props => {

    const navigate = useNavigate();
    const classes = useStyles();


    const onLogin = () => {
        localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
        navigate(AppRoutes.home);
    }

    var cardMapping = [
        {
            number: "14",
            label: "Active Properties",
            img: "images/greenHouse.svg"
        },
        {
            number: "06",
            label: "Blocks",
            img: "images/Leads.png",
        },
        {
            number: "12",
            label: "Floors",
            img: "images/parking.svg",

        },
        {
            number: "14",
            label: "Residents",
            img: "images/sandHome.svg",

        },
        {
            number: "10",
            label: "Active Unit",
            img: "images/blueHouse.png",

        },
        {
            number: "03",
            label: "Vacant",
            img: "images/tickHouse.png",
        },
        {
            number: "17",
            label: "Reserved",
            img: "images/LeadSeoncd.png",

        },
        {
            number: "45",
            label: "Occupied",
            img: "images/active.png"
        },
    ]
    var graphicalCard = [
        {
            label: "Property Types",
            img: "images/enlarge.png",
            graphImg: "images/propertyType.png"
        },
        {
            label: "Unit Category",
            img: "images/enlarge.png",
            graphImg: "images/unitCat.png"
        },
        {
            label: "Vacant Units By Property",
            img: "images/enlarge.png",
            graphImg: "images/vacant.png"
        },
        {
            label: "Total Area",
            img: "images/enlarge.png",
            graphImg: "images/totalArea.png"
        }
    ]
    var generalRequest = [
        {
            title: "Water Leakage Repair",
            subTitle: "Maintenance",
            date: "22 Jan 21",
            codeId: "K-F01-U207"
        },
        {
            title: "Electricity Volatage Drop",
            subTitle: "Maintenance",
            date: "18 Jan 21",
            codeId: "K-F01-U977"
        },
        {
            title: "Gas Leakage Repair",
            subTitle: "Maintenance",
            date: "28 Jan 22",
            codeId: "K-F02-U222"
        },
        {
            title: "Water Leakage Repair",
            subTitle: "Maintenance",
            date: "11 Jan 21",
            codeId: "K-L01-U877"
        },
        {
            title: "Electricity Volatage Drop",
            subTitle: "Maintenance",
            date: "01 Jan 22",
            codeId: "K-K01-U277"
        },

    ]
    var maintenance = [
        {
            title: "Gas Leakage Repair",
            subTitle: "Maintenance",
            date: "22 Jan 21",
            codeId: "K-F01-U277"
        },
        {
            title: "Water Leakage Repair",
            subTitle: "Maintenance",
            date: "22 Jan 21",
            codeId: "K-F01-U277"
        },
        {
            title: "Electricity Volatage Drop",
            subTitle: "Maintenance",
            date: "22 Jan 21",
            codeId: "K-F01-U277"
        },
        {
            title: "Water Leakage Repair",
            subTitle: "Maintenance",
            date: "22 Jan 21",
            codeId: "K-F01-U277"
        },
        {
            title: "Electricity Volatage Drop",
            subTitle: "Maintenance",
            date: "22 Jan 21",
            codeId: "K-F01-U277"
        },

    ]
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return <Box className={classes.root}>
        {/* card with details */}
        <Box className={classes.createPortion}>
            <Grid container spacing={2} >
                {cardMapping?.map((val) => {
                    return (
                        <Grid item xs={6} sm={4} md={1.5} lg={1.5}>
                            <Box className={classes.propertyDetails}>
                                <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} mb={2} >
                                    <Typography className={classes.cardNumber}>{val.number}</Typography>
                                    <img src={val.img} alt="img" className={classes.cardImg} />
                                </Stack>
                                <Typography className={classes.cardLabel}>{val.label}</Typography>
                            </Box>
                        </Grid>
                    )
                })
                }
            </Grid>
        </Box>
        {/* card with details */}
        {/* graphical card */}
        <Box className={classes.createGraphCard}>
            <Grid container spacing={2} >
                {graphicalCard?.map((val) => {
                    return (
                        <Grid item xs={12} sm={6} md={3} lg={3}>
                            <GraphCard val={val} />
                        </Grid>
                    )
                })
                }
            </Grid>
        </Box>
        {/* graphical card */}
        {/* requests/maintaince and property table */}
        <Box className={classes.createGraphCard}>
            <Grid container spacing={2} >
                {/* requests/maintaince */}
                <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Box className={classes.requestDetails}>
                        <Box className={classes.requetsBox} p={2} pb={0}>
                            <Stack direction={"row"} alignItems={"center"} className={classes.parentRequest} divider={<Divider orientation="vertical" flexItem className={classes.borderRightStyling} />} spacing={2} >
                                <Box mr={2}>
                                    <Typography className={classes.generalReq} pb={0.5}>General Requests</Typography>
                                    <Typography className={classes.generalNumber}>12</Typography>
                                </Box>
                                <Box>
                                    <Typography className={classes.generalReq} pb={0.5}>Maintaince</Typography>
                                    <Typography className={classes.generalNumber}>15</Typography>
                                </Box>
                            </Stack>
                        </Box>
                        <Box sx={{ width: '100%' }}>
                            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" className={classes.tabParent}>
                                    <Tab className={classes.switchingTab} label="General Requests" {...a11yProps(0)} />
                                    <Tab className={classes.switchingTab} label="Maintenance" {...a11yProps(1)} />
                                </Tabs>
                            </Box>
                            {/* general request tab */}
                            <TabPanel value={value} index={0}>
                                <Box className={classes.searchParent}>
                                    <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pb={1.5}>
                                        <Typography className={classes.requestNumbers}>General Requests (12)</Typography>
                                        <Typography className={classes.viewAll}>View All</Typography>
                                    </Stack>
                                    <Stack mb={1.5}>
                                        <Search />
                                    </Stack>
                                    <Stack className={classes.overflowTypes}>
                                        {generalRequest?.map((val) => {
                                            return (
                                                <>
                                                    <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pb={1.5}>
                                                        <Box>
                                                            <Typography className={classes.waterLeak} mb={0.6}>{val?.title}</Typography>
                                                            <Typography className={classes.dateAndType} display={"flex"} alignItems={"center"}>{val?.subTitle}<span className={classes.dot}></span>{val?.date}<span className={classes.dot}></span>{val?.codeId}</Typography>
                                                        </Box>
                                                        <Box display={"flex"} alignItems={"center"} >
                                                            <Stack><Outline /></Stack>
                                                            <Stack ml={2}><ShowingIcon /></Stack>
                                                        </Box>
                                                    </Stack>
                                                </>
                                            )
                                        })
                                        }
                                    </Stack>
                                </Box>
                            </TabPanel>
                            {/* general request tab */}
                            {/* maintenance tab */}
                            <TabPanel value={value} index={1}>
                                <Box className={classes.searchParent}>
                                    <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pb={1.5}>
                                        <Typography className={classes.requestNumbers}>Maintenance (15)</Typography>
                                        <Typography className={classes.viewAll}>View All</Typography>
                                    </Stack>
                                    <Stack mb={1.5}>
                                        <Search />
                                    </Stack>
                                    <Stack className={classes.overflowTypes}>
                                        {maintenance?.map((val) => {
                                            return (
                                                <>
                                                    <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pb={1.5}>
                                                        <Box>
                                                            <Typography className={classes.waterLeak} mb={0.6}>{val?.title}</Typography>
                                                            <Typography className={classes.dateAndType} display={"flex"} alignItems={"center"}>{val?.subTitle}<span className={classes.dot}></span>{val?.date}<span className={classes.dot}></span>{val?.codeId}</Typography>
                                                        </Box>
                                                        <Box display={"flex"} alignItems={"center"} >
                                                            <Stack><Outline /></Stack>
                                                            <Stack ml={2}><ShowingIcon /></Stack>
                                                        </Box>
                                                    </Stack>
                                                </>
                                            )
                                        })
                                        }
                                    </Stack>
                                </Box>
                            </TabPanel>
                            {/* maintenance tab */}
                        </Box>
                    </Box>
                </Grid>
                {/* requests/maintaince */}
                {/* property table */}
                <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Box className={classes.propertyDetails}>
                        <Stack>
                            <Typography className={classes.tableLabel} pb={2}>Occupancy By Property</Typography>
                            <Stack className={classes.tableWithAddIcon}>
                                <CustomizeTable />
                                <Box className={classes.addIconButton}>
                                    <AddIconImg sx={{width: "64px", height: "64px"}} onClick={onLogin} />
                                </Box>
                            </Stack>
                        </Stack>
                    </Box>
                </Grid>
                {/* property table */}
            </Grid >
        </Box >
        {/* requests/maintaince and property table */}
    </Box >
}