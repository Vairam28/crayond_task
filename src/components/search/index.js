import React from 'react';
import { TextField, Stack } from '@mui/material';
import { makeStyles } from "@mui/styles";
import Box from '@mui/material/Box';
import SearchIcon from '@mui/icons-material/Search';
import { useStyles } from "./style";


export const Search = ({

}) => {

    const classes = useStyles();

    return <Box className={classes.root}>
        <Stack direction="row">
            <Box className={classes.parentSearch} >
                <SearchIcon className={classes.searchIcon} />
            </Box>
            <TextField id="outlined-basic" placeholder='Search' variant="outlined" className={classes.textField} />
        </Stack>
    </Box>

}