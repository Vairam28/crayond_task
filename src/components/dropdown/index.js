import React from 'react';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { useStyles } from "./style";
import ExpandMoreSharpIcon from '@mui/icons-material/ExpandMoreSharp';

export const Dropdown = ({
    label = {},
    placeholder = {}

}) => {

    const classes = useStyles();

    const [age, setAge] = React.useState('');

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return <Box className={classes.root}>
        <FormControl className={classes.dropdownIcon}>
            <label className={classes.label}>{label}</label>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={age}
                onChange={handleChange}
                displayEmpty
                className={classes.dropdown}
                size={'small'}
            >
                <MenuItem value="">{placeholder}</MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
            </Select>
        </FormControl>
    </Box>

}