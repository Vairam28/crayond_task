import React from 'react';
import { useStyles } from "./style";
import { AppBar, Toolbar, IconButton, Typography, Drawer, Stack, Box, Divider, Avatar } from '@mui/material';
import { Menu as MenuIcon } from '@mui/icons-material';
import { LocalStorageKeys } from '../../../utils';
import { SideNavBar } from '..';
import { Notification } from '../../../assets/notificationIcon';


export const TopNavBar = (props) => {

    const classes = useStyles();

    const [state, setState] = React.useState({
        openSideNavBar: false
    })

    const handleLogout = () => {

    }

    const toogleSideNavBar = () => {
        setState({
            ...state,
            openSideNavBar: !state.openSideNavBar
        })
    }

    return (
        <div className={classes.grow}>
            <AppBar position="static" className={classes.appBar}>
                <Toolbar className={classes.toolbarHeight}>

                    {/* <IconButton className={classes.menuIcon} onClick={toogleSideNavBar} size="large">
                        <MenuIcon htmlColor="white" />
                    </IconButton> */}

                    <Stack direction="row"
                        divider={<Divider orientation="vertical" flexItem sx={{borderColor: "white",height: "15px", margin: "auto !important"}} />}
                        spacing={2}
                        className={classes.titleContainer}
                        alignItems="center" >
                        {/* logo */}
                        <IconButton>
                            <img src="images/DNT Logo White-04@2x.png" alt="logo" className={classes.logoTop} />
                        </IconButton>
                        {/* logo */}

                        <Typography className={classes.management}>
                            PROPERTY MANAGEMENT SOLUTION
                        </Typography>
                    </Stack>

                    <div className={classes.grow} />
                    <Stack direction="row"
                        divider={<Divider orientation="vertical" flexItem sx={{borderColor: "white",height: "22px", margin: "auto !important"}} />}
                        spacing={1}
                        alignItems="center"
                        className={classes.navbarTopLeftDropdown}
                        >
                        {/* notificationIcon */}
                        <IconButton
                            aria-label="logout button"
                            aria-controls={"logout_button"}
                            aria-haspopup="true"
                            onClick={handleLogout}
                            color="inherit"
                            size="small">
                            <Notification />
                        </IconButton>
                        {/* notificationIcon */}
                        {/* avatar */}
                        <Stack direction="row" alignItems="center" spacing={1}>
                        <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg"  sx={{ width: 31, height: 31 }}/>
                         {/* avatar */}
                         {/* signOut */}
                         <Box alignItems="center" >
                            <Typography variant='body1' className={classes.adminName}>
                            Bala Ganesh
                            </Typography>
                            <Typography variant="body2" className={classes.adminOccupation}>
                            Super Admin
                            </Typography>
                         </Box>
                         {/* signOut */}
                         {/* dropdownIcon */}
                        <IconButton>
                        <img src="images/downExpand.png" alt="expandDown" />
                        </IconButton>
                         {/* dropdownIcon */}
                         </Stack>
                    </Stack>
                    <Drawer
                        open={state.openSideNavBar}
                        variant={"temporary"}
                        anchor="left"
                        onClose={toogleSideNavBar}>
                        <div style={{ width: 240 }}>
                            <SideNavBar isMobile={true} />
                        </div>
                    </Drawer>

                </Toolbar>
            </AppBar>
        </div>
    );
}
