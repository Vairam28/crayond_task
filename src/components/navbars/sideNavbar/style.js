import { makeStyles } from "@mui/styles";
const drawerWidth = 56;
export const useStyles = makeStyles((theme) => ({
    root: {
        width: props => props?.isMobile ? 240 : drawerWidth,
        position: 'absolute',
    },
    drawer: {
        height: props => props?.isMobile ? `100vh` : `calc(100vh - 64px)`,
        width: props => props?.isMobile ? 240 : drawerWidth,
        width: "72px",
        height: "100vh",
        backgroundColor:"#333333",
        position:"fixed",
        marginTop:"57px"
    },
    // drawerContainer: {
    //     overflow: 'hidden',
    // },
}))