import React from 'react';
import { useStyles } from "./style";
import { Paper, List, ListItem, ListItemIcon, ListItemText, Typography, Stack, Divider } from '@mui/material';
import { matchPath, useNavigate, useLocation } from 'react-router-dom';
import { DownArrow } from '../../../assets/downArrow';
import { FourBoxIcon } from '../../../assets/fourBoxIcon'
import { Leads } from "../../../assets/leads"

export const SideNavBar = (props) => {

    const classes = useStyles(props);

    const navigate = useNavigate();
    const location = useLocation();

    const isSelected = (data) => {
        if (data.link) {
            return matchPath(location.pathname, {
                path: data.link
            })
        }
    }

    const onListClick = (data) => {
        if (data.link) {
            navigate(data.link)
        }
    }

    return (
        <div className={classes.root}>
            <Paper
                className={classes.drawer}
                square
            >
                <div className={classes.drawerContainer}>
                    <List>
                        {[].map((navBar, index) => (
                            <ListItem onClick={(e) => onListClick(navBar)}
                                button
                                key={index}
                                selected={isSelected(navBar)}>

                                <ListItemIcon>{navBar.icon}</ListItemIcon>

                                <ListItemText primary={navBar.name} />

                            </ListItem>
                        ))}
                    </List>
                </div>
                <Stack direction="column"
                    divider={<Divider orientation="horizontal" flexItem sx={{borderColor: "white",width: "21px",margin:"auto !important",marginTop:"23px !important"}}/>} spacing={2} alignItems="center" >
                    <DownArrow />
                    <Stack direction="column" spacing={1} alignItems="center">
                        <FourBoxIcon />
                        <Leads />
                    </Stack>
                </Stack>
            </Paper>
        </div>
    );
}
