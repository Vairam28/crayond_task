import React from 'react';
import { TextField, Stack,InputAdornment } from '@mui/material';
import Box from '@mui/material/Box';
import { useStyles } from "./style";



export const TextBox = ({
    label = "",
    placeholder = "",
    disable = "",
    adornment = ""
}) => {


    const classes = useStyles();

    return <Box className={classes.root}>
        <Stack direction="column" spacing={1}>
            <label className={classes.label}>{label??""}</label>
            <TextField disabled={disable?? ""} size={'small'}  id="outlined-basic" variant="outlined" placeholder={placeholder ?? ""} className={classes.textBox} InputProps={{ endAdornment: <InputAdornment className={classes.inputAdornment} position="end">{adornment?? ""}</InputAdornment>, }}/>
        </Stack>
    </Box>

}