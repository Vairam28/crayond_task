import React from 'react';
import { Box, Stack, Typography} from '@mui/material';
import { useStyles } from "./style";


export const GraphCard = ({
    val = {}

}) => {

    const classes = useStyles();

    return <Box className={classes.root} sx={{height:"100%"}} >
        <Box className={classes.cardParent}>
            <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pl={1} pr={1} >
                <Typography className={classes.graphLabel}>{val?.label}</Typography>
                <img src={val?.img} alt="enclose" className={classes.encloseIcon} />
            </Stack>
            <Stack p={2}>
                <img src={val?.graphImg} alt="imgs" />
            </Stack>
        </Box>
    </Box>

}