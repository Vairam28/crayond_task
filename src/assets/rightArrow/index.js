import * as React from "react"

export const rightArrow = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={10.5}
    height={5.538}
    {...props}
  >
    <path
      d="M9.437.416 5.25 4.239 1.063.416a.509.509 0 0 0-.673 0 .408.408 0 0 0 0 .615l4.524 4.13a.509.509 0 0 0 .673 0l4.524-4.13a.408.408 0 0 0 0-.615.509.509 0 0 0-.674 0Z"
      fill="#fff"
      stroke="#fff"
      strokeWidth={0.5}
    />
  </svg>
)

