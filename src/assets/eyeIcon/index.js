import * as React from "react"

export const ShowingIcon = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    data-name="Component 43 \u2013 4"
    width={28}
    height={28}
    {...props}
  >
    <rect
      data-name="Rectangle 44485"
      width={28}
      height={28}
      rx={6}
      fill="none"
    />
    <path
      data-name="icons8-eye (5)"
      d="M14 8c-5.818 0-8 6-8 6s2.182 6 8 6 8-6 8-6-2.182-6-8-6Zm0 1.5c3.837 0 5.778 3.2 6.406 4.5-.629 1.288-2.584 4.5-6.406 4.5-3.837 0-5.778-3.2-6.406-4.5.63-1.285 2.584-4.5 6.406-4.5Zm0 1.5a3 3 0 0 0 0 6 3 3 0 0 0 0-6Zm0 1.5a1.5 1.5 0 1 1-1.455 1.5A1.477 1.477 0 0 1 14 12.5Z"
      fill="#98a0ac"
    />
  </svg>
)

