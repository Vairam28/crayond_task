import * as React from "react"

export const Notification = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    data-name="Component 85 \u2013 1"
    width={29}
    height={29}
    {...props}
  >
    <rect
      data-name="Rectangle 44485"
      width={29}
      height={29}
      rx={6}
      fill="none"
    />
    <g data-name="Group 838">
      <path
        d="M14.635 8.006a5.477 5.477 0 0 0-5.126 5.515v2.609l-.951 1.914v.011a1.272 1.272 0 0 0 1.137 1.791h3.051a2.154 2.154 0 0 0 4.308 0h3.05a1.273 1.273 0 0 0 1.137-1.791v-.011l-.951-1.914v-2.746a5.394 5.394 0 0 0-5.655-5.378Zm.051 1.076a4.3 4.3 0 0 1 4.517 4.3v2.872a.539.539 0 0 0 .056.24l1 2.018a.167.167 0 0 1-.163.255H9.69a.167.167 0 0 1-.162-.255l1-2.017a.539.539 0 0 0 .056-.24v-2.734a4.4 4.4 0 0 1 4.101-4.44Zm-.868 10.764h2.154a1.077 1.077 0 1 1-2.154 0Z"
        fill="#fff"
      />
      <g
        data-name="Ellipse 40005"
        transform="translate(16.001 6)"
        fill="#5078e1"
        stroke="#071741"
        strokeWidth={1.5}
      >
        <circle cx={3} cy={3} r={3} stroke="none" />
        <circle cx={3} cy={3} r={3.75} fill="none" />
      </g>
    </g>
  </svg>
)

