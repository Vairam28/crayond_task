import React from "react";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { AppRoutes } from "./routes";
import PrivateRouter from "./privateRouter";

import {
  NotFound,
  Home,
  Login,
  UpdatedScreen,
  DashboardCard,
  PropTable
} from './../screens';

const RouterApp = (props) => {
  
  return (
    <BrowserRouter>
      <Routes>

        {/* Home Route */}
        <Route path={AppRoutes.home} element={<Home /> } />

        {/* Login Route */}
        <Route path={AppRoutes.login} element={<Login />} />

         {/* updated screen Route */}
         <Route path={AppRoutes.updatedScreen} element={<UpdatedScreen />  } />

          {/* updated screen Route */}
          <Route path={AppRoutes.dashboardCard} element={<DashboardCard />  } />

           {/* updated screen Route */}
           <Route path={AppRoutes.propTable} element={<PropTable />  } />

        {/* For unknow/non-defined path */}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RouterApp;
